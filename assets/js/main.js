(function($) {
    window.onload = function() {
        $(document).ready(function() {
            info_epidemic();
            hospitalImage();
            hospitalVideo();
            handbookCarousel();
            activityCarousel();
            datePicker();
            backToTop();
            menuMobile();
            boxSearch();
            ShowAdDiv();
            choosse_font_detail();
        });
    };
})(jQuery);

function info_epidemic() {
    $('.info__epidemic-carousel').owlCarousel({
        loop: true,
        margin: 10,
        nav: true,
        dots: false,
        autoplay: true,
        autoplayTimeout: 8000,
        responsiveClass: true,
        dotsEach: true,
        smartSpeed: 450,
        responsive: {
            0: {
                items: 1,
                nav: true,
                dots: false,
            },
            576: {
                items: 1,
                nav: true,
                dots: false,
            },
            768: {
                items: 1,
                nav: true,
                dots: false,
            },
            1024: {
                items: 1,
                nav: true,
                dots: false,
            },

            1400: {
                items: 1,
                nav: true,
                dots: false,
            }
        }
    })
}

function hospitalImage() {
    $('.hospitalImage-carousel').owlCarousel({
        loop: true,
        margin: 30,
        nav: true,
        dots: false,
        autoplay: true,
        autoplayTimeout: 5000,
        responsiveClass: true,
        dotsEach: true,
        smartSpeed: 450,
        responsive: {
            0: {
                items: 1,
                nav: true,
                dots: true,
            },
            576: {
                items: 2,
                nav: true,
                dots: false,
            },
            768: {
                items: 2,
                nav: true,
                dots: false,
            },
            1024: {
                items: 3,
                nav: true,
                dots: false,
            },

            1400: {
                items: 3,
                nav: true,
                dots: false,
            }
        }
    })
}

function hospitalVideo() {
    $('.hospitalVideo-carousel').owlCarousel({
        loop: true,
        margin: 30,
        nav: true,
        dots: false,
        autoplay: true,
        autoplayTimeout: 5000,
        responsiveClass: true,
        dotsEach: true,
        smartSpeed: 450,
        responsive: {
            0: {
                items: 1,
                nav: true,
                dots: false,
            },
            576: {
                items: 1,
                nav: true,
                dots: false,
            },
            768: {
                items: 1,
                nav: true,
                dots: false,
            },
            1024: {
                items: 1,
                nav: true,
                dots: false,
            },

            1400: {
                items: 1,
                nav: true,
                dots: false,
            }
        }
    })
}

function handbookCarousel() {
    $('.handbook-carousel').owlCarousel({
        loop: true,
        margin: 30,
        nav: true,
        dots: false,
        autoplay: true,
        autoplayTimeout: 5000,
        responsiveClass: true,
        dotsEach: true,
        smartSpeed: 450,
        responsive: {
            0: {
                items: 1,
                nav: true,
                dots: false,
            },
            576: {
                items: 1,
                nav: true,
                dots: false,
            },
            768: {
                items: 1,
                nav: true,
                dots: false,
            },
            1024: {
                items: 1,
                nav: true,
                dots: false,
            },

            1400: {
                items: 1,
                nav: true,
                dots: false,
            }
        }
    })
}

function activityCarousel() {
    $('.activity-carousel').owlCarousel({
        loop: true,
        margin: 30,
        nav: true,
        dots: false,
        autoplay: true,
        autoplayTimeout: 5000,
        responsiveClass: true,
        dotsEach: true,
        smartSpeed: 450,
        responsive: {
            0: {
                items: 1,
                nav: true,
                dots: false,
            },
            576: {
                items: 1,
                nav: true,
                dots: false,
            },
            768: {
                items: 1,
                nav: true,
                dots: false,
            },
            1024: {
                items: 1,
                nav: true,
                dots: false,
            },

            1400: {
                items: 1,
                nav: true,
                dots: false,
            }
        }
    })
}

function datePicker() {
    var d = new Date();

    var Calendar = {
        themonth: d.getMonth(), // The number of the month 0-11
        theyear: d.getFullYear(), // This year
        today: [d.getFullYear(), d.getMonth(), d.getDate()], // adds today style
        selectedDate: null, // set to today in init()
        years: [], // populated with last 10 years in init()
        months: ['Tháng 1', 'Tháng 2', 'Tháng 3', 'Tháng 4', 'Tháng 5', 'Tháng 6', 'Tháng 7', 'Tháng 8', 'Tháng 9', 'Tháng 10', 'Tháng 11', 'Tháng 12'],

        init: function() {
            this.selectedDate = this.today
                // Populate the list of years in the month/year pulldown
            var year = this.theyear;
            for (var i = 0; i < 10; i++) {
                this.years.push(year--);
            }

            this.bindUIActions();
            this.render();
        },

        bindUIActions: function() {
            // Create Years list and add to ympicker
            for (var i = 0; i < this.years.length; i++)
                $('<li>' + this.years[i] + '</li>').appendTo('.calendar-ympicker-years');
            this.selectMonth();
            this.selectYear(); // Add active class to current month n year

            // Slide down year month picker
            $('.calendar-header').click(function() {
                $('.calendar-ympicker').css('transform', 'translateY(0)');
            });

            // Close year month picker without action
            $('.close').click(function() {
                $('.calendar-ympicker').css('transform', 'translateY(-100%)');
            });

            // Move calander to today
            $('.today').click(function() {
                Calendar.themonth = d.getMonth();
                Calendar.theyear = d.getFullYear();
                Calendar.selectMonth();
                Calendar.selectYear();
                Calendar.selectedDate = Calendar.today;
                Calendar.render();
                $('.calendar-ympicker').css('transform', 'translateY(-100%)');
            });

            // Click handlers for ympicker list items
            $('.calendar-ympicker-months li').click(function() {
                Calendar.themonth = $('.calendar-ympicker-months li').index($(this));
                Calendar.selectMonth();
                Calendar.render();
                $('.calendar-ympicker').css('transform', 'translateY(-100%)');
            });
            $('.calendar-ympicker-years li').click(function() {
                Calendar.theyear = parseInt($(this).text());
                Calendar.selectYear();
                Calendar.render();
                $('.calendar-ympicker').css('transform', 'translateY(-100%)');
            });

            // Move the calendar pages
            $('.minusmonth').click(function() {
                Calendar.themonth += -1;
                Calendar.changeMonth();
            });
            $('.addmonth').click(function() {
                Calendar.themonth += 1;
                Calendar.changeMonth();
            });
        },

        // Adds class="active" to the selected month/year
        selectMonth: function() {
            $('.calendar-ympicker-months li').removeClass('active');
            $('.calendar-ympicker-months li:nth-child(' + (this.themonth + 1) + ')').addClass('active');
        },
        selectYear: function() {
            $('.calendar-ympicker-years li').removeClass('active');
            $('.calendar-ympicker-years li:nth-child(' + (this.years.indexOf(this.theyear) + 1) + ')').addClass('active');
        },

        // Makes sure that month rolls over years correctly
        changeMonth: function() {
            if (this.themonth == 12) {
                this.themonth = 0;
                this.theyear++;
                this.selectYear();
            } else if (this.themonth == -1) {
                this.themonth = 11;
                this.theyear--;
                this.selectYear();
            }
            this.selectMonth();
            this.render();
        },

        // Helper functions for time calculations
        TimeCalc: {
            firstDay: function(month, year) {
                var fday = new Date(year, month, 1).getDay(); // Mon 1 ... Sat 6, Sun 0
                if (fday === 0) fday = 7;
                return fday - 1; // Mon 0 ... Sat 5, Sun 6
            },
            numDays: function(month, year) {
                return new Date(year, month + 1, 0).getDate(); // Day 0 is the last day in the previous month
            }
        },

        render: function() {
            var days = this.TimeCalc.numDays(this.themonth, this.theyear), // get number of days in the month
                fDay = this.TimeCalc.firstDay(this.themonth, this.theyear), // find what day of the week the 1st lands on        
                daysHTML = '',
                i;

            $('.calendar div.monthname').text(this.months[this.themonth] + '  ' + this.theyear); // add month name and year to calendar
            for (i = 0; i < fDay; i++) { // place the first day of the month in the correct position
                daysHTML += '<li class="noclick">&nbsp;</li>';
            }
            // write out the days
            for (i = 1; i <= days; i++) {
                if (this.today[0] == this.selectedDate[0] &&
                    this.today[1] == this.selectedDate[1] &&
                    this.today[2] == this.selectedDate[2] &&
                    this.today[0] == this.theyear &&
                    this.today[1] == this.themonth &&
                    this.today[2] == i)
                    daysHTML += '<li class="active today">' + i + '</li>';
                else if (this.today[0] == this.theyear &&
                    this.today[1] == this.themonth &&
                    this.today[2] == i)
                    daysHTML += '<li class="today">' + i + '</li>';
                else if (this.selectedDate[0] == this.theyear &&
                    this.selectedDate[1] == this.themonth &&
                    this.selectedDate[2] == i)
                    daysHTML += '<li class="active">' + i + '</li>';
                else
                    daysHTML += '<li>' + i + '</li>';

                $('.calendar-body').html(daysHTML); // Only one append call
            }

            // Adds active class to date when clicked
            $('.calendar-body li').click(function() { // toggle selected dates
                if (!$(this).hasClass('noclick')) {
                    $('.calendar-body li').removeClass('active');
                    $(this).addClass('active');
                    Calendar.selectedDate = [Calendar.theyear, Calendar.themonth, $(this).text()]; // save date for reselecting
                }
            });
        }
    };

    Calendar.init();
}

function menuMobile() {
    var $main_nav = $('#main-nav');
    var $toggle = $('.toggle');

    var defaultData = {
        maxWidth: false,
        customToggle: $toggle,
        // navTitle: 'All Categories',
        levelTitles: true,
        pushContent: '#container'
    };

    // add new items to original nav
    $main_nav.find('li.add').children('a').on('click', function() {
        var $this = $(this);
        var $li = $this.parent();
        var items = eval('(' + $this.attr('data-add') + ')');

        $li.before('<li class="new"><a>' + items[0] + '</a></li>');

        items.shift();

        if (!items.length) {
            $li.remove();
        } else {
            $this.attr('data-add', JSON.stringify(items));
        }

        Nav.update(true);
    });

    // call our plugin
    var Nav = $main_nav.hcOffcanvasNav(defaultData);

    // demo settings update

    const update = (settings) => {
        if (Nav.isOpen()) {
            Nav.on('close.once', function() {
                Nav.update(settings);
                Nav.open();
            });

            Nav.close();
        } else {
            Nav.update(settings);
        }
    };

    $('.actions').find('a').on('click', function(e) {
        e.preventDefault();

        var $this = $(this).addClass('active');
        var $siblings = $this.parent().siblings().children('a').removeClass('active');
        var settings = eval('(' + $this.data('demo') + ')');

        update(settings);
    });

    $('.actions').find('input').on('change', function() {
        var $this = $(this);
        var settings = eval('(' + $this.data('demo') + ')');

        if ($this.is(':checked')) {
            update(settings);
        } else {
            var removeData = {};
            $.each(settings, function(index, value) {
                removeData[index] = false;
            });

            update(removeData);
        }
    });
}

function boxSearch() {
    var btn_op_search = document.querySelector('#search');
    var over_lay_search = document.querySelector('.over-lay-mb');
    var box_search_hd = document.querySelector('.box-search_header');
    

    btn_op_search.onclick = function() {
        over_lay_search.classList.add('overlay-open');
        box_search_hd.classList.add('active_show');
    }

    over_lay_search.onclick = function() {
        over_lay_search.classList.remove('overlay-open');
        box_search_hd.classList.remove('active_show');
    }

    var input_ss_cc = document.querySelector('.box-search_header form input');
    var btn_search = document.querySelector('.btn-hd-search'); 
    if(btn_search == null){
        return 0;
    }
    else{
        btn_search.onclick = function(){
            
            if(input_ss_cc.value == ""){
                alert("Vui lòng nhập từ khoá");
                preventDefault();
            }
        }
    }

}

function backToTop() {
    var backToTop = document.querySelector('.backToTop');
    backToTop.addEventListener('click', function() {
        window.scroll({
            top: 0,
        })
    })
}

function FloatTopDiv() {
    startLX = ((document.body.clientWidth - MainContentW) / 2) - (LeftBannerW + LeftAdjust), startLY = TopAdjust;
    startRX = ((document.body.clientWidth - MainContentW) / 2) + (MainContentW + RightAdjust), startRY = TopAdjust;
    var d = document;

    function ml(id) {
        var el = d.getElementById ? d.getElementById(id) : d.all ? d.all[id] : d.layers[id];
        el.sP = function(x, y) {
            this.style.left = x + 'px';
            this.style.top = y + 'px';
        };
        el.x = startRX;
        el.y = startRY;
        return el;
    }

    function m2(id) {
        var e2 = d.getElementById ? d.getElementById(id) : d.all ? d.all[id] : d.layers[id];
        e2.sP = function(x, y) {
            this.style.left = x + 'px';
            this.style.top = y + 'px';
        };
        e2.x = startLX;
        e2.y = startLY;
        return e2;
    }
    window.stayTopLeft = function() {
        if (document.documentElement && document.documentElement.scrollTop)
            var pY = document.documentElement.scrollTop;
        else if (document.body)
            var pY = document.body.scrollTop;
        if (document.body.scrollTop > 30) {
            startLY = 3;
            startRY = 3;
        } else {
            startLY = TopAdjust;
            startRY = TopAdjust;
        };
        ftlObj.y += (pY + startRY - ftlObj.y) / 16;
        ftlObj.sP(ftlObj.x, ftlObj.y);
        ftlObj2.y += (pY + startLY - ftlObj2.y) / 16;
        ftlObj2.sP(ftlObj2.x, ftlObj2.y);
        setTimeout("stayTopLeft()", 1);
    }
    ftlObj = ml("divAdRight");
    //stayTopLeft(); 
    ftlObj2 = m2("divAdLeft");
    stayTopLeft();
}

function ShowAdDiv() {
    var objAdDivRight = document.getElementById("divAdRight");
    var objAdDivLeft = document.getElementById("divAdLeft");
    if (objAdDivLeft == null && objAdDivRight == null) {
        return 0;
    } else {
        objAdDivRight.style.display = "block";
        objAdDivLeft.style.display = "block";
        FloatTopDiv();
    }
}

function choosse_font_detail(){
    var btn_plus_font = document.querySelector('#plus-f-dt');
    var btn_minus_font = document.querySelector('#minus-f-dt');

    if(btn_plus_font == null || btn_minus_font == null){
        return 0;
    }

    else{
        var the_content = document.querySelector('.main-detail .the_content');
        var style = window.getComputedStyle(the_content, null).getPropertyValue('font-size');
        var fontSize = parseFloat(style); 
        btn_plus_font.addEventListener('click', function(){
            the_content.style.fontSize = (fontSize ++) + 'px';
        });

        btn_minus_font.addEventListener('click', function(){
            the_content.style.fontSize = (fontSize --) + 'px';

            if(the_content.style.fontSize == 10){
                return 0;
            }
        });
    }
}